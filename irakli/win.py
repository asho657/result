import configparser
import re
import winrm
from datetime import datetime
from errbot import BotPlugin, botcmd, arg_botcmd
from requests.exceptions import ConnectionError


class win(BotPlugin):
    @arg_botcmd("-c", dest='host', type=str, default=None)
    @arg_botcmd("-k", dest='key', type=str, default=None)
    def run(self, msg, host, key):
        if key is None or host is None:
            yield "host or key not specified"
            return
        keys = []
        with open("../../config/commands") as file:
            for line in file:
                line = line.lower()
                if ':' not in line:
                    continue

                ind = line.find(":")
                keyword = line[:ind].strip()
                keys.append(keyword)

                if keyword == key:
                    command = line[ind + 1:].strip()
                    output = self.connect_to_windows(host, command)
                    yield output
                    return
            yield 'wrong keyword: ' + key + ' .try these:'
            for k in keys:
                yield k
            return

    @botcmd
    def service(self, msg, args):
        yield "got it! wait a bit..."
        command = re.findall('[^,\s]+', str(args))  # get args
        if not command:
            yield "umm, (notsureif) what should I do? you need to specify the command" \
                  "\n command pattern: systemName YY-MM-DD , YY-MM-DD ..."
            return
        sysName = command[0]  # get computer name from the args
        if re.match('^[0-9]', sysName):
            yield "please specify a system name in a correct format, i.e not stating with a digit :)"
            return

        args = args.replace("/", "-")
        dates = re.findall('[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}', args)  # rest of the command(dates)

        if not dates:
            yield "you should tell me what files to look for (pokerface) I am not that smart yet ;)"
            return

        dates = sorted(set(dates))

        formatted_dates = []
        for date in dates:
            try:
                datetime_object = datetime.strptime(date, '%Y-%m-%d')
            except:
                yield "wrong date format for {}. I have to skip it :(".format(date)
                continue
            formatted_dates.append(datetime.strftime(datetime_object, '%Y-%m-%d'))

        if not formatted_dates:
            yield "all dates passed were in wrong format, sorry :("
            return

        import os.path
        path = '/home/mais/errbot-root/config/winconfig.ini'
        if not os.path.exists(path):
            yield "configuration file could not be found :("
            return
        parser = configparser.ConfigParser()
        parser.read(path)  # read config file

        dates = str(formatted_dates).strip("[]").replace(" ", "")

        try:
            ps_script = """powershell "C:\\bot\\log-service.ps1" \
                -mainComp {} \
                -dirOnMainComp {} \
                -mainCurrentDir {} \
                -altComp {} \
                -dirOnAltComp {} \
                -destComp {} \
                -dirOnDestComp {} \
                -fileName {} \
                -configDir {} \
                -dates {}
            """.format(
                parser[sysName]['mainComp'],
                parser[sysName]['dirOnMainComp'],
                parser[sysName]['mainCurrentDir'],
                parser[sysName]['altComp'],
                parser[sysName]['dirOnAltComp'],
                parser[sysName]['destComp'],
                parser[sysName]['dirOnDestComp'],
                parser[sysName]['fileName'],
                parser[sysName]['configDir'],
                dates
            )
        except:
            yield "system name is wrong or something is wrong with config file :("
            return

        yield self.connect_to_windows('172.16.0.197', ps_script)

    # connect to windows and return command output as string
    def connect_to_windows(self, host, command):
        try:
            s = winrm.Session(host, auth=("administrator", "Aa1234"))
            r = s.run_ps(command)
        except:
            self.log.exception("log--------------")
            return 'could not connect. maybe trying again will fix it ;)'
        if r.status_code == 0:
            return r.std_out.decode("utf-8").replace("\\r\\n", "\r\n")
        else:
            return "something went wrong while processing your request"