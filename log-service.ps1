﻿param([string]$mainComp,[string]$dirOnMainComp,[string]$mainCurrentDir,[string]$configDir,
        [string]$altComp,[string]$dirOnAltComp, 
        [string]$destComp, [string]$dirOnDestComp,
        [string]$fileName, [string[]]$dates) 

if(!($mainCurrentDir.EndsWith('\') -or $mainCurrentDir.EndsWith('/'))){
    $mainCurrentDir = $mainCurrentDir + "\"
}
if(!($dirOnMainComp.EndsWith('\') -or $dirOnMainComp.EndsWith('/'))){
    $dirOnMainComp = $dirOnMainComp + "\"
}
if(!($dirOnDestComp.EndsWith('\') -or $dirOnDestComp.EndsWith('/'))){
    $dirOnDestComp = $dirOnDestComp + "\"
}
if(!($dirOnAltComp.EndsWith('\') -or $dirOnAltComp.EndsWith('/'))){
    $dirOnAltComp = $dirOnAltComp + "\"
}

if(!($configDir.EndsWith('\') -or $configDir.EndsWith('/'))){
    $configDir = $configDir + "\"
}

# set : ConvertTo-SecureString -String "password" -AsPlainText -Force  | ConvertFrom-SecureString | Out-File "DIR\password.txt"
$password = Get-Content $configDir"password.txt" | ConvertTo-SecureString

# set : "Username"  | Out-File "DIR\username.txt"
$username = Get-Content $configDir"username.txt"

if(!$password -or !$username){
    echo "credential files not found"
    return
}

function CreateSession
{
    param([string]$address)
    #$password =  ConvertTo-SecureString -String $pass -AsPlainText -Force
    $cred = New-Object System.Management.Automation.PSCredential("$address\$username",$password)
    $sess = New-PSSession -ComputerName $address -Credential $cred

    return $sess
}

function TransferFile([string]$path,[string]$destination,$file, $address, $user, $pass,$from=$null)
{
    
    if($from -ne $null){
        invoke-command -ScriptBlock ${function:TransferFile} -Session $from -ArgumentList $path,$destination, $file,$address, $user , $pass
    }else{
       # $password =  ConvertTo-SecureString -String $pass -AsPlainText -Force
        $cred = New-Object System.Management.Automation.PSCredential("$address\$user",$pass)
        $sess = New-PSSession -ComputerName $address -Credential $cred
        Copy-Item -ToSession $sess -Path "$path$file"  -Destination $destination 
        Remove-PSSession $sess
    }
}

function Contains
{
    param([string]$file, [string] $rar, $session = $null)

    if($session -ne $null){
        invoke-command -ScriptBlock ${function:Contains} -Session $session -ArgumentList $file, $rar
    }else{
        $res = & "C:\Program Files\WinRAR\Rar.exe" i $rar $file
        $res -icontains "Found  $rar / $file"
    }
}



function Exists($path, $session = $null)
{
    if($session -ne $null){
        invoke-command -ScriptBlock ${function:Exists} -Session $session -ArgumentList $path
    }else{
        Test-Path $path
    }
}

function Process($path,$file,$session)
{
    $exists = Exists -path "$path$file.rar" -session $session
    if($exists){
        $contains = Contains  -file "$file" -rar "$path$file.rar" -session $session
        return $contains
    }
    return $false
}

$main = CreateSession -address $mainComp
if(!$main){
    echo "$mainComp is not accessible"
    return
}

$alt = CreateSession -address $altComp
if(!$alt){
    echo "$altComp is not accessible"
    return
}

$dest = CreateSession -address $destComp
if(!$dest){
    echo "$destComp is not accessible"
    return
}


function MakeRar($date, $currentDir, $tmpDir, $file){
    Copy-Item -Destination $tmpDir -Path $currentDir$file

    $make = & "C:\Program Files\WinRAR\rar.exe" a -df -ep -r "$tmpDir$file.$date.rar"  $tmpDir$file
    
}

function IsCurrentDate($date){
    $d = Get-Date $date 
    $c = (Get-Date).Date
    return $d -eq $c
}

foreach($date in $dates){
    echo "\r\n \r\n"

    if(IsCurrentDate($date)){
        $date = Get-Date -Format yy-MM-ddThh-mm-ss
        invoke-command -ScriptBlock ${function:MakeRar} -Session $main -ArgumentList $date,$mainCurrentDir,$dirOnMainComp,$fileName
       
        TransferFile -path $dirOnMainComp -destination $dirOnDestComp -file "$filename.$date.rar" -address $destComp -user $username -pass $password  -from $main
        
        echo "[success] $fileName.$date.rar file was transfered from main($mainComp) to dest($destComp)"
        
        continue;
    }

    $onDest = Process -path $dirOnDestComp -file "$fileName.$date" -session $dest
    if($onDest){
       echo "[success] $fileName.$date.rar already exists on dest($destComp)"
       continue
    }
    #echo "$dirOnDestComp$fileName.$date.rar doesn't exist on destination computer $destComp"

    $onMain = Process -path $dirOnMainComp -file "$fileName.$date" -session $main
    if($onMain){
       #echo "$dirOnMainComp$fileName.$date.rar exists on main computer $mainComp}"
       TransferFile -path $dirOnMainComp -destination $dirOnDestComp -file "$fileName.$date.rar" -address $destComp -user $username -pass $password  -from $main

       echo "[success] $fileName.$date.rar was transfered from main($mainComp) to dest($destComp)"
       
       continue
    }
    #echo "$dirOnMainComp$fileName.$date.rar doesn't exist on main computer $mainComp"

    $onAlt = Process -path $dirOnAltComp -file "$fileName.$date" -session $alt
    if($onAlt){
        #echo "$dirOnAltComp$fileName.$date.rar exists on alternative computer ${$alt.ComputerName}"
        TransferFile -path $dirOnAltComp -destination $dirOnDestComp -file "$fileName.$date.rar" -address $destComp -user $username -pass $password  -from $alt 
        $send = Exists -path $dirOnDestComp$fileName -session $to
        
        echo "[success] $fileName.$date.rar was transfered from alt($altComp) to dest($destComp)"
        
        continue
    }
    #echo "$dirOnAltComp$fileName.$date.rar doesn't exist on alternative computer $altComp"
    echo "[error] $fileName.$date not found!!!" 
}



Remove-PSSession $main
Remove-PSSession $alt
Remove-PSSession $dest