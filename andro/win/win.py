from errbot import BotPlugin, botcmd, arg_botcmd
import winrm
from requests.exceptions import ConnectionError
from winrm.exceptions import InvalidCredentialsError


class Win(BotPlugin):
    @botcmd()
    def service(self, msg, args):

        import re
        import configparser
        import os, os.path
        from datetime import datetime
        from configparser import ConfigParser

        if not args:
            yield "umm, (notsureif) what should I do? you need to specify command like: \nsystemName YYYY-MM-DD, ... , YYYY-MM-DD"
            return

        names = re.findall(r'[a-zA-Z@0-9-]+', args)

        if not names:
            yield "umm, (notsureif) what should I do? you need to specify command like: \nsystemName YYYY-MM-DD, ... , YYYY-MM-DD"
            return

        systemName = names[0]
        args = args[args.find(names[0]) + len(names[0]):];

        if re.match(r"^[0-9]", systemName):
            yield "system name mustn't start with digit (unknown)"
            return

        args = args.replace('/', '-')
        dates = re.findall(r'[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}', args)
        files = [];

        for date in sorted(set(dates)):
            try:
                files.append(datetime.strptime(date, "%Y-%m-%d").strftime("%Y-%m-%d"))
            except:
                yield "(failed) wrong date format '{}'. format should be YYYY-MM-DD".format(date)

        if not files:
            yield " (failed) you should specify dates. \n I'm not Chuck Norris  (chucknorris)"
            return

        path = os.environ.get("SERVICES_CONFIG", None);
        if path is None or not os.path.exists(path):
            yield "(failed) configuration file not found, check if SERVICES_CONFIG is defined correctly."
            return

        config = ConfigParser()
        config.read(path)

        farr = ''
        for x in files:
            farr += x + ","
        datesArr = farr.strip(",")

        try:
            script = """powershell "C:\\bot\\log-service.ps1" \
            -configDir {} \
            -mainComp {}  \
            -dirOnMainComp {}  \
            -mainCurrentDir {} \
            -altComp {}  \
            -dirOnAltComp {}  \
            -destComp {} \
            -dirOnDestComp {}  \
            -fileName {}  \
            -dates {}  \
            """.format(
                config[systemName]['configDir'],
                config[systemName]['mainComp'],
                config[systemName]['dirOnMainComp'],
                config[systemName]['mainCurrentDir'],
                config[systemName]['altComp'],
                config[systemName]['dirOnAltComp'],
                config[systemName]['destComp'],
                config[systemName]['dirOnDestComp'],
                config[systemName]['fileName'],
                datesArr
            )
        except KeyError:
            yield "(failed) configuration for system \"{}\" is not defined correctly, please check \"{}\" file.".format(
                systemName, path)
            return

        yield "(gangnamstyle) processing..."
        
        res = self.remote_job("172.16.0.197", script)
        if (res['status'] == 1):
            yield "(failed) powershell script not found :|"
        else:
            result = res['out'].replace("[error]", "(failed)").replace("[success]", "(successful)")
            yield "service {} {} (successful) \n {} ".format(systemName, datesArr, result)

    @arg_botcmd("-c", dest="hostname", type=str, default=None)  # ip or domain
    @arg_botcmd("-k", dest='keyword', type=str, default=None)  # keyword
    def exec(self, msg, hostname, keyword):
        if hostname is None:
            if keyword is None:
                yield "hostname and keyword are not specified."
            else:
                yield "hostname is not specified."
            yield "usage: exec [-h] [-c HOSTNAME] [-k KEYWORD]"
            return

        if keyword is None:
            yield "keyword is not specified."
            yield "usage: exec [-h] [-k KEYWORD] [-c HOSTNAME]"
            return

        import os
        path = os.environ.get("COMMANDS_FILE", None)
        if path is None:
            yield "Environment variable COMMANDS_FILE is not defined."
            return

        try:
            file = open(path, 'r')
            keys = []
            for line in file:
                ind = line.find(":")
                if ind == -1:
                    continue

                line.lower()
                key = line[:ind].strip()
                keys.append(key)
                if key == keyword:
                    command = line[ind + 1:].strip()
                    yield "connecting. please wait a while..."
                    result = self.remote_job(hostname, command)
                    yield result['out']
                    return

            yield 'keyword "{}" not found. possible keywords are :\n'.format(keyword)
            for k in keys:
                yield k
        except:
            yield 'file  "{}" is not accessible right now. please try again later.'.format(path)

    def remote_job(self, host, command, credentials=("Administrator", "Aa1234")):
        try:
            session = winrm.Session(host, auth=credentials)
            response = session.run_ps(command)
        except ConnectionError:
            return {'out': 'cannot connect to {}. please check the hostname and try again'.format(host),
                    'status': '404'}
        except InvalidCredentialsError:
            return {'out': "you don't have access to {}.".format(host), 'status': '401'}

        if response.status_code == 0:
            return {'out': response.std_out.decode("utf-8").replace("\\r\\n", "\r\n"), 'status': 0}
        else:
            return {'out': response.std_err.decode("utf-8"), 'status': response.status_code}
